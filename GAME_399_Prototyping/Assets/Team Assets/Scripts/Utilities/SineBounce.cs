﻿using UnityEngine;

public class SineBounce : MonoBehaviour
{
    [Tooltip("Multiply Movement Speeds.")]
    public float MovementSpeedMult;
    [Tooltip("Direction to move in (Angle)")]
    public float MovementDirection;
    [Tooltip("If Sine Movement is used, define this parameter.")]
    public float amplitude;
    [Tooltip("If Sine Movement is used, define this parameter.")]
    public float period;

    void Update()
    {
            transform.localPosition += (Quaternion.Euler(new Vector3(0.0f, 0.0f, MovementDirection)) * transform.right)
                * (amplitude * (Mathf.Sin(((2 * Mathf.PI) * period) * Time.time) * Time.deltaTime)) * (MovementSpeedMult);
    }
}