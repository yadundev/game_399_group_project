﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplaceUV : MonoBehaviour
{
    public Vector2 Displacement;
    public Renderer ObRender;
	void Start ()
    {ObRender = GetComponent<Renderer>();}

	void Update ()
    {ObRender.material.mainTextureOffset += Displacement*Time.deltaTime;}
}
