﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public Transform ObstacleMover;

    private KarmaPool[] pools;

    /// <summary>
    /// what was the last object spawned
    /// </summary>
    public GameObject lastSpawned;
    /// <summary>
    /// The pool that the last obstacle was spawned up.
    /// </summary>
    public KarmaPool lastPool;
    /// <summary>
    /// how many consecutive instances of the same object did it spawn
    /// </summary>
    private int consecutiveSpawns;
    /// <summary>
    /// how long has the player been alive (so we can scale difficulty of generation)
    /// </summary>
    public float runLength;
    /// <summary>
    /// At what point in runLength was the last object spawned.
    /// </summary>
    private float lastTime;
    /// <summary>
    /// Should a new object be spawned?
    /// </summary>
    [HideInInspector]
    public bool tospawn;

    private void Start()
    {
        consecutiveSpawns = 0;
        lastSpawned = null;
        runLength = 0;
        pools = FindObjectsOfType<KarmaPool>();
    }
    private void FixedUpdate()
    {
        tospawn = true;
        runLength += Time.fixedDeltaTime;
    }
    private void LateUpdate()
    {
        
        
        if (tospawn)
        { SpawnfromPool(); }
    }

    private void SpawnfromPool()
    {
        //GET OBSTACLE//
        KarmaPool spool = PerformKarma();
        GameObject obstacle = spool.GetFromPool();

        //SET OBSTACLE POSITION//
        obstacle.transform.position = PointonPlatform(transform.position, transform.localScale);

        //SET OBSTACLE PARENT TO THE MOVER//
        obstacle.transform.SetParent(ObstacleMover,true);

        //CONSECUTIVE SPAWN LOGIC//
        if (lastPool != null)
        {
            if (spool.karmaData.self.ID == lastPool.karmaData.self.ID)
            {consecutiveSpawns++;}
            else
            { consecutiveSpawns = 0; }
        }
        else
        { consecutiveSpawns = 0; }

        //SET LAST SPAWNED INFO//
        lastPool = spool;
        lastSpawned = obstacle;
        tospawn = false;
    }
    
    private static Vector3 PointonPlatform(Vector3 center, Vector3 size)
    {return center;}
    private void OnTriggerEnter(Collider other)
    {tospawn = false;}
    private void OnTriggerStay(Collider other)
    {tospawn = false; }
    private void OnTriggerExit(Collider other)
    {tospawn = true;}

    private KarmaPool PerformKarma()
    {
        float highScore = float.NegativeInfinity;
        int recordHolder = -1;
        float cf = 0;
        for (int i = 0; i < pools.Length;i++)
        {
            cf = KarmaEvaluation(pools[i].karmaData);
            if (cf > highScore)
            { recordHolder = i; highScore = cf; }
        }
        if (recordHolder == -1)
        { throw new System.Exception("NO KARMA POOL IS VALID!!! DEFINE THEM IN THE INSPECTOR!"); }
        else if (recordHolder > pools.Length)
        { throw new System.IndexOutOfRangeException("THE HIGHEST KARMA DOESNT EXIST! HOW DID THIS EVEN HAPPEN!"); }
        else
        {return pools[recordHolder];}
    }
    public float KarmaEvaluation(KarmaData kdata)
    {
        float result = 0;
        if (lastPool != null)
        {
            if (lastPool.karmaData.self.ID != kdata.self.ID)
            {
                for (int i = 0; i < kdata.othersInfluence.Length; i++)
                {
                    if (kdata.othersInfluence[i].ID == lastPool.karmaData.self.ID)
                        result += SimpleKarmaEvaluation(kdata.othersInfluence[i]);
                }
            }
        }
        result+= KarmaEvaluation(kdata.self); 
        return result;
    }
    public float KarmaEvaluation(Karma kd)
    {
        float result = kd.baseKarma;
        result += runLength * (1f / kd.runLengthFactor);
        if (lastPool != null)
        {
            if (lastPool.karmaData.self.ID == kd.ID)
            { result += consecutiveSpawns * (1f / kd.consecutiveObjectFactor); }
        }
        foreach (float f in kd.randomnessFactors)
        {
            result += Random.value * f;
        }
        return result;
    }
    public float SimpleKarmaEvaluation(SimpleKarma kd)
    {
        float result = kd.baseKarma;
        result += runLength * (1f / kd.runLengthFactor);
        return result;
    }
}
