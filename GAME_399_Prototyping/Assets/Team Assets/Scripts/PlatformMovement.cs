﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour
{
    public Vector3 moveSpeed;
    public Transform startPoint;
    public Transform endPoint;
    public float minimumDistance;

    private void Update()
    {
        if (Vector3.Distance(transform.position,endPoint.position) <= minimumDistance)
        { ResetPosition(); }
    }
    void FixedUpdate ()
    {
        transform.position += moveSpeed * Time.fixedDeltaTime;
	}
    private void ResetPosition()
    {
        List<Vector3> positions = new List<Vector3>();
        List<GameObject> childs = new List<GameObject>();
        for(int i = 0; i < transform.childCount;i++)
        {
            childs.Add(transform.GetChild(i).gameObject);
            positions.Add(transform.GetChild(i).position);
        }
        transform.position = startPoint.position;
        for(int i = 0; i < childs.Count;i++)
        {
            childs[i].transform.position = positions[i];
        }
        childs.Clear();
        positions.Clear();
    }
}
