﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantSpin : MonoBehaviour
{
    public Vector3 spd;
    private void FixedUpdate()
    {transform.localRotation *= Quaternion.Euler(spd.x * Time.fixedDeltaTime, spd.y * Time.fixedDeltaTime, spd.z * Time.fixedDeltaTime);}
}
