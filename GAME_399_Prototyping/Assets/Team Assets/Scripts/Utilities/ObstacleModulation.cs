﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleModulation : MonoBehaviour
{
    [SerializeField]
    private Vector3 startPos;
    [SerializeField]
    private Vector3 randomPos;
    [SerializeField]
    private Vector3 startScale;
    [SerializeField]
    private Vector3 randomScale;    
    [SerializeField]
    private Vector3 startEuler;
    [SerializeField]
    private Vector3 randomEuler;

    private bool hasBeenDisabled;

    private void OnDisable()
    {
        hasBeenDisabled = true;
    }
    void OnEnable()
    {
        if (hasBeenDisabled == true)
        {
            transform.localPosition = startPos;
            transform.localScale = startScale;
            transform.localEulerAngles = startEuler;
        }
        if (randomScale != Vector3.zero)
        { transform.localScale = startScale + (new Vector3(randomScale.x * Random.value, randomScale.y * Random.value, randomScale.z * Random.value)); }
        if (randomPos != Vector3.zero)
        { transform.localPosition = startPos + (new Vector3(randomPos.x * Random.value, randomPos.y * Random.value, randomPos.z * Random.value)); }
        if (randomEuler != Vector3.zero)
        { transform.localEulerAngles = startEuler + (new Vector3(randomEuler.x * Random.value, randomEuler.y * Random.value, randomEuler.z * Random.value)); }
    }
}
