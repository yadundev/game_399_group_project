﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class KarmaPool : MonoBehaviour
{
    [Header("Name for identification.")]
    public string obstacleName;
    [Header("RNG related data. Don't ignore.")]
    public KarmaData karmaData;
    [Header("The prefab to pool.")]
	[SerializeField]
	private GameObject prefab;
	//this is a waiting line for Objects
	private Queue<GameObject> availableObjects = new Queue<GameObject>();

	private void Awake()
	{
        GrowPool ();
    }

	public GameObject GetFromPool()
	{
		if (availableObjects.Count == 0)
			GrowPool ();

		var instance = availableObjects.Dequeue ();
		instance.SetActive (true);
        instance.gameObject.name = obstacleName;
		return instance;
	}

	private void GrowPool()
	{
		for (int i = 0; i < 10; i++) {
			var instanceToAdd = Instantiate (prefab);
			instanceToAdd.transform.SetParent (transform);
			AddToPool (instanceToAdd);
		}
	}

	public void AddToPool(GameObject instance)
	{
		instance.SetActive (false);
		availableObjects.Enqueue (instance);
	}
}
[System.Serializable]
public struct KarmaData
{
    
    public Karma self;
    [Header("Here we can modify the chance that this object appears after others.")]
    [Header("Not manditory.")]
    [Tooltip("When this KarmaPool is tested, it factors these values based on the last spawned from pool.")]
    public SimpleKarma[] othersInfluence;
}
    
[System.Serializable]
public struct Karma
{
    public int ID;
    [Header("Base chance")]
    public float baseKarma;
    [Tooltip("Chance multiplier that factors time survived, in seconds.")]
    [Rename("Run Length Factor (1/value)")]
    public float runLengthFactor;
    [Tooltip("Chance multiplier applied per consecutive spawn from this pool. Generally keep this below 0.")]
    [Rename("Consecutive Instances Factor (1/value)")]
    public float consecutiveObjectFactor;
    [Tooltip("Additive multipliers (+=randomNessFactors[x]*Random.value)")]
    [Rename("Randomness Factors (1/value)")]
    public float[] randomnessFactors;
}
[System.Serializable]
public struct SimpleKarma
{
    public int ID;
    public float baseKarma;
    public float runLengthFactor;
}