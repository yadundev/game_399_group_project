﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDespawner : MonoBehaviour
{
    public float minDistance;
    private KarmaPool[] pools;
    private Transform mover;

    private void Start()
    {
        pools = FindObjectsOfType<KarmaPool>();
        mover = FindObjectOfType<PlatformMovement>().transform;
    }
    private void FixedUpdate()
    {
        for (int i = 0; i < mover.childCount;i++)
        {
            if (Vector3.Distance(transform.position,mover.GetChild(i).transform.position) <= minDistance)
            {
                DespawnObject(mover.GetChild(i).gameObject);
            }
        }
    }
    private void DespawnObject(GameObject ob)
	{
        for (int i = 0; i < pools.Length;i++)
        {
            if (pools[i].obstacleName == ob.name)
            {
                ob.transform.SetParent(null, true);
                pools[i].AddToPool(ob);
                return;
            }
        }
    }
}
